import Vue from 'vue'
import Vuex from 'vuex'

import app from '@/modules/app/store'
import user from '@/modules/user/store'
import security from '@/modules/security/store'
import sector from '@/modules/sector/store'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		app: app,
		user: user,
		security: security,
		sector: sector
	}
})
