const get = function(key) {
	let data = localStorage.getItem(key)
	if (data) {
		return JSON.parse(data)['value']
	} else {
		return null
	}
}

const set = function(key, value) {
	localStorage.setItem(
		key,
		JSON.stringify({
			value: value
		})
	)
}

const del = function(key) {
	localStorage.removeItem(key)
}

export default { get, set, del }
