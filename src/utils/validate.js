import i18n from '@/plugins/i18n'

const required = value => !!value || i18n.t('validate.required')

const email = value => {
	const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
	return pattern.test(value) || i18n.t('validate.email')
}
const minSize = len => v => (v || '').length >= len || i18n.t('validate.minSize', {min: len})
const maxSize = len => v => (v || '').length <= len || i18n.t('validate.maxSize', {max: len})

export default {
	required,
	email,
	minSize,
	maxSize
}
