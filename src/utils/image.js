const profile = function (image) {
	return (image) ? `${process.env.VUE_APP_CDN}/${image}`
		: 'https://www.gravatar.com/avatar/00000000000000000000000000000000?d=mp&f=y'
}

export default { profile }