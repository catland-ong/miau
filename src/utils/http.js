import axios from 'axios'
import noty from '@/utils/noty'
import NProgress from 'nprogress'
import LocalStorage from '@/utils/local-storage'

const http = axios.create({
	baseURL: process.env.VUE_APP_API,
	timeout: 10000
})

http.interceptors.request.use(config => {

	NProgress.start()

	const token = LocalStorage.get('token')

	if (token) {
		config.headers = {'Authorization': `Bearer ${token}`}
	}

	return config
})

http.interceptors.response.use(
	resp => {
		NProgress.done()
		if (resp && resp.data) {
			return resp.data
		} else {
			return resp
		}
	},
	err => {

		NProgress.done()

		if (err.response && err.response.status === 400) {
			noty.error(err.response.data.msg, JSON.parse(err.response.config.data))
		} else if (err.response && err.response.status === 401) {
			noty.error('error.security.unauthorized')
		}
		else {
			noty.error('error.internalServerError')
		}

	}
)

export default http