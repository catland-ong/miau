import Noty from 'noty'
import 'noty/lib/noty.css'
import 'noty/lib/themes/metroui.css'
import i18n from '@/plugins/i18n'

const options = {
	theme: 'metroui',
	timeout: 4000,
	layout: 'topRight',
	closeWith: ['click', 'button']
}

const error = function(key, data) {
	options.type = 'error'
	options.text = i18n.t(key, data)
	new Noty(options).show()
}

const success = function (key, data) {
	options.type = 'success'
	options.text = i18n.t(key, data)
	new Noty(options).show()
}

export default { success, error }