import colors from 'vuetify/lib/util/colors'

export default {
	primary: colors.pink,
	secondary: colors.indigo,
	error: colors.red.darken2
}