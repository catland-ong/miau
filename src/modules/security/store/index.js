import http from '@/utils/http'
import router from '@/router'
import LocalStorage from '@/utils/local-storage'

export default {
	namespaced: true,
	actions: {
		login: async (context, user) => {

			const data = await http.post('/v1/security/login', user).then(data => {
				return data
			})

			if (data) {
				LocalStorage.set('token', data.token)
			}

			return data
		},
		forgotPassword: async (context, user) => {
			return http.post('/v1/security/forgot-password', user)
		},
		changePassword: async (context, user) => {
			return http.post('/v1/security/change-password', user)
		},
		logout: async (context) => {
			LocalStorage.del('token')
			router.push({path: '/security/login'})
		}
	}
}
