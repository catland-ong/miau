export default {
	path: '/security',
	component: () => import('@/modules/security/layout/Security'),
	children: [
		{
			path: 'login',
			name: 'security.login',
			component: () => import('@/modules/security/views/Login')
		},
		{
			path: 'forgot-password',
			name: 'security.forgotPassword',
			component: () => import('@/modules/security/views/Forgot')
		},
		{
			path: 'forgot-password/:hash',
			name: 'security.changePassword',
			component: () => import('@/modules/security/views/ChangePassword')
		}
	]
}
