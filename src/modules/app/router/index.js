export default {
	path: '/',
	component: () => import('@/modules/app/layout/App'),
	children: [
		{
			path: '',
			name: 'app',
			component: () => import('@/modules/app/views/App'),
			meta: { auth: true }
		}
	]
}
