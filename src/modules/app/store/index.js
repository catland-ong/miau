export default {
	namespaced: true,
	state: {
		sidebarOpen: true
	},
	mutations: {
		sidebar: state => state.sidebarOpen = !state.sidebarOpen
	}
}
