export default {
	path: '/',
	component: () => import('@/modules/app/layout/App'),
	children: [
		{
			path: 'user',
			name: 'user',
			component: () => import('@/modules/user/views/User'),
			meta: { auth: true }
		},
		{
			path: 'add/user',
			name: 'add-user',
			component: () => import('@/modules/user/views/AddUser'),
			meta: { auth: true }
		},
		{
			path: 'edit/user/:userId',
			name: 'edit-user',
			component: () => import('@/modules/user/views/EditUser'),
			meta: { auth: true }
		}
	]
}
