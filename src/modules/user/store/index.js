import http from '@/utils/http'
import LocaStorage from '@/utils/local-storage'
import jwtDecode from 'jwt-decode'
import image from '@/utils/image'

export default {
	namespaced: true,
	state: {
		user: null
	},
	getters: {

		user: state => {
			if (state.user) {
				return state.user
			}
			const user = jwtDecode(LocaStorage.get('token'))
			user.gravatar = image.profile(user.image)
			state.user = user
			return user
		}

	},
	actions: {
		byIdUser: async (context, userId) => {
			return http.get(`/v1/user/${userId}`)
		},
		listUser: async () => {
			return http.get('/v1/user')
		},
		addUser: async (context, user) => {
			delete user.image
			return http.post('/v1/user', user)
		},
		editUser: async (context, user) => {
			return http.put('/v1/user', user)
		},
		addUserProfileImage: async (context, file) => {
			const formData = new FormData()
			formData.append('id', file.userId)
			formData.append('file', file.file)
			return http.post('/v1/user/upload/image', formData, {headers: {'Content-Type': 'multipart/form-data'}})
		}
	}
}
