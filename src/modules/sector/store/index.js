import http from '@/utils/http'

export default {
	namespaced: true,
	actions: {
		addSector: async (context, sector) => {
			return http.post('/v1/sector', sector)
		},
		editSector: async (context, sector) => {
			return http.put(`/v1/sector/${sector.id}`, sector)
		},
		findById: async (context, id) => {
			return http.get(`/v1/sector/${id}`)
		},
		listSector: async (context) => {
			return http.get('/v1/sector')
		}
	}
}
