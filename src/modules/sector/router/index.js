export default {
	path: '/',
	component: () => import('@/modules/app/layout/App'),
	children: [
		{
			path: 'sector',
			name: 'sector',
			component: () => import('@/modules/sector/views/Sector'),
			meta: { auth: true }
		},
		{
			path: 'add/sector',
			name: 'add-sector',
			component: () => import('@/modules/sector/views/AddSector'),
			meta: { auth: true }
		},
		{
			path: 'edit/sector/:sectorId',
			name: 'edit-sector',
			component: () => import('@/modules/sector/views/EditSector'),
			meta: { auth: true }
		}
	]
}
