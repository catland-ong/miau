export default {
	path: '*',
	name: '404',
	component: () => import('@/modules/error/views/Error404')
}