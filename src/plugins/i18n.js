import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

import pt from '@/i18n/pt'

const messages = {
	pt
}

export default new VueI18n({
	locale: 'pt',
	messages
})