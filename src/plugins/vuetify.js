import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import i18n from '@/plugins/i18n'
import themes from '@/theme'

Vue.use(Vuetify)

export default new Vuetify({
	lang: { t: (key, ...params) => i18n.t(key, params)},
	theme: {themes},
	icons: { iconfont: 'mdi' },
})
