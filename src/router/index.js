import Vue from 'vue'
import Router from 'vue-router'
import NProgress from 'nprogress'

import app from '@/modules/app/router'
import error from '@/modules/error/router'
import security from '@/modules/security/router'
import user from '@/modules/user/router'
import sector from '@/modules/sector/router'

import LocalStorage from '@/utils/local-storage'

Vue.use(Router)

const router = new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [app, error, security, user, sector]
})

router.beforeEach((to, from, next) => {

	NProgress.start()

	const loggedIn = LocalStorage.get('token')

	if ((
		to.name === 'security.login'
		|| to.name === 'security.forgotPassword'
		|| to.name === 'security.changePassword'
	)
		&& loggedIn) {

		next('/')

	}
	else if (to.matched.some(record => record.meta.auth) && !loggedIn) {
		next({
			path: '/security/login',
			query: { redirect: to.fullPath }
		})
	}
	else {
		next()
	}

})

router.afterEach((to, from) => {
	NProgress.done()
})

export default router
