export default {
	validate: {
		required: 'Preenchimento obrigatório',
		email: 'Email inválido',
		minSize: 'Quantidade mínima de caracteres: {min}',
		maxSize: 'Quantidade máxima de caracteres: {max}'
	}
}