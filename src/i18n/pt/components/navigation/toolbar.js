export default {
	components: {
		navigation: {
			toolbar: {
				title: 'Miau',
				rightMenu: {
					item: {
						logout: {
							text: 'Sair'
						}
					}
				}
			}
		}
	}
}