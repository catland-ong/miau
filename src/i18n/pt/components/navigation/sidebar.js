export default {
	components: {
		navigation: {
			sidebar: {
				leftMenu: {
					item: {
						news: {text: 'Notícias'},
						dashboard: {text: 'Dashboard'},
						user: {text: 'Usuários'},
						sector: {text: 'Setor'}
					}
				}
			}
		}
	}
}