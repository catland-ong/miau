export default {
	error: {
		internalServerError: 'Ops! Algo de errado não esta certo, entre em contato com o administrador...',
		validation: 'Ops, algum dado não foi preenchido corretamente'
	}
}