import merge from 'lodash.merge'

// utils
import error from '@/i18n/pt/error'
import validate from '@/i18n/pt/validate'
import vuetify from '@/i18n/pt/vuetify'

// components
import sidebar from '@/i18n/pt/components/navigation/sidebar'
import toolbar from '@/i18n/pt/components/navigation/toolbar'

// security
import login from '@/i18n/pt/modules/security/login'
import forgot from '@/i18n/pt/modules/security/forgot'
import changePassword from '@/i18n/pt/modules/security/change-password'

// user
import user from '@/i18n/pt/modules/user/user'
import addUser from '@/i18n/pt/modules/user/add-user'
import editUser from '@/i18n/pt/modules/user/edit-user'

// sector
import sector from '@/i18n/pt/modules/sector/sector'
import addSector from '@/i18n/pt/modules/sector/add-sector'
import editSector from '@/i18n/pt/modules/sector/edit-sector'

export default merge(
	error,
	validate,
	vuetify,
	sidebar,
	toolbar,
	login,
	forgot,
	changePassword,
	user,
	addUser,
	editUser,
	sector,
	addSector,
	editSector
)