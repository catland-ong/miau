export default {
	security: {
		changePassword: {
			form: {
				title: 'Miau - Alteração de senha',
				link: {
					goToLogin: 'Voltar para o login'
				},
				fields: {
					password: {
						label: 'Digite uma nova senha'
					},
					confirmPassword: {
						label: 'Digite a senha novamente'
					}
				},
				actions: {
					changePassword: 'Alterar minha senha'
				},
				error: {
					forgotPasswordExpired: 'Ops! Parece que essa solicitação expirou, tente recuperar sua senha novamente'
				},
				success: 'Sua senha foi alterada com sucesso, você já pode acessar o Miau'
			}
		}
	}
}