export default {
	security: {
		login: {
			title: 'Miau',
			form: {
				link: {
					goToForgot: 'Esqueci minha senha'
				},
				fields: {
					email: {
						label: 'Digite seu email'
					},
					password: {
						label: 'Digite sua senha'
					}
				},
				actions: {
					btn: {
						access: 'Acessar'
					}
				},
				error: {
					emailOrPassword: 'Email ou senha incorretos'
				}
			}
		}
	}
}