export default {
	security: {
		forgot: {
			form: {
				title: 'Miau - Recuperar minha senha',
				link: {
					goToLogin: 'Voltar para o login'
				},
				fields: {
					email: {
						label: 'Digite seu email'
					}
				},
				actions: {
					forgot: 'Recuperar senha'
				},
				success: 'Encaminhamos um email para lhe ajudar a recupera sua senha',
				error: {
					emailNotfound: 'Ops! Não encontramos o seu email'
				}
			}
		}
	}
}