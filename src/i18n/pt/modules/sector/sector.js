export default {
	sector: {
		title: 'Setores',
		fields: {
			search: {
				label: 'Busca por nome'
			}
		},
		actions: {
			add: {
				tooltip: 'Adicionar novo setor'
			}
		},
		dataTable: {
			headers: {
				name: 'Nome',
				description: 'Descrição'
			},
			actions: {
				edit: {
					tooltip: 'Editar'
				}
			}
		}
	}
}