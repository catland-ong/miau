export default {
	add: {
		sector: {
			title: 'Adicionar setor',
			actions: {
				goToSector: 'Voltar para todos os setores'
			},
			form: {
				fields: {
					name: {
						label: 'Digite um nome para o setor'
					},
					description: {
						label: 'Digite uma descrição para o setor'
					}
				},
				actions: {
					add: 'Adicionar setor'
				},
				success: 'Setor "<b>{name}</b>" adicionado com sucesso',
				error: {
					alreadyExists: 'Ops! o setor "<b>{name}</b>" já existe'
				},
			}
		}
	}
}