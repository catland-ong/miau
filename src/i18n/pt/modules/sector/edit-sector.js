export default {
	edit: {
		sector: {
			title: 'Editar setor',
			actions: {
				goToSector: 'Voltar para todos os setores'
			},
			form: {
				fields: {
					name: {
						label: 'Digite um nome para o setor'
					},
					description: {
						label: 'Digite uma descrição para o setor'
					}
				},
				actions: {
					edit: 'Editar setor'
				},
				success: 'Setor "<b>{name}</b>" editado com sucesso',
				error: {
					alreadyExists: 'Ops! o setor "<b>{name}</b>" já existe'
				},
			}
		}
	}
}