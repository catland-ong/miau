export default {
	user: {
		title: 'Usuários',
		fields: {
			search: {
				label: 'Busca por nome'
			}
		},
		actions: {
			add: {
				tooltip: 'Adicionar usuário'
			}
		},
		dataTable: {
			headers: {
				name: 'Nome',
				email: 'Email'
			},
			actions: {
				edit: {
					tooltip: 'Editar'
				}
			}
		},
		form: {
			fields: {
				firstName: {
					label: 'Nome'
				},
				lastName: {
					label: 'Sobrenome'
				},
				email: {
					label: 'Email'
				},
				cpf: {
					label: 'Cpf'
				},
				rg: {
					label: 'Rg'
				},
				birthday: {
					label: 'Data de nascimento',
					actions: {
						cancel: 'Cancelar',
						save: 'Salvar'
					}
				},
				phone: {
					label: 'Telefone'
				},
				cellPhone: {
					label: 'Celular'
				},
				address: {
					label: 'Endereço'
				},
				number: {
					label: 'Número'
				},
				complement: {
					label: 'Complemento'
				},
				district: {
					label: 'Bairro'
				},
				city: {
					label: 'Cidade'
				},
				state: {
					label: 'Estado'
				},
				country: {
					label: 'País'
				},
				postalCode: {
					label: 'Cep'
				},
				sector: {
					label: 'Área de atuação'
				},
				facebook: {
					label: 'Facebook'
				},
				linkedin: {
					label: 'Linkedin'
				},
				car: {
					label: 'Possuí carro?'
				},
				cat: {
					label: 'Quantos gatos?'
				},
				howDidYouMeet: {
					label: 'Como conheceu a ong?'
				}
			},
		}
	}
}