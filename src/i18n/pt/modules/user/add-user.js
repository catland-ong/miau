export default {
	add: {
		user: {
			title: 'Adicionar usuário',
			actions: {
				goToUser: 'Voltar para usuários'
			},
			form: {
				actions: {
					add: 'Adicionar usuário'
				},
				success: 'Usuário "<b>{firstName}</b>" adicionado com sucesso',
				error: {
					alreadyExists: 'Ops! o usuário "<b>{firstName}</b>" já existe'
				},
			}
		}
	}
}