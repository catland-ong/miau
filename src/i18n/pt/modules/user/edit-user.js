export default {
	edit: {
		user: {
			title: 'Editar usuário',
			actions: {
				goToUser: 'Voltar para usuários'
			},
			form: {
				actions: {
					edit: 'Editar usuário'
				},
				success: 'Usuário "<b>{firstName}</b>" editado com sucesso',
				error: {
					alreadyExists: 'Ops! o usuário "<b>{firstName}</b>" já existe'
				},
			}
		}
	}
}