[![GitHub stars](https://img.shields.io/github/stars/cat-land/miau-ui.svg?style=flat-square)](https://github.com/cat-land/miau-ui/stargazers)
[![Build Status](https://travis-ci.org/cat-land/miau-ui.svg?branch=master)](https://travis-ci.org/cat-land/miau-ui)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/65deef1427bc415e834a7a839ba31ee9)](https://www.codacy.com/manual/cat-land/miau-ui?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=cat-land/miau-ui&amp;utm_campaign=Badge_Grade)
[![GitHub issues](https://img.shields.io/github/issues/cat-land/miau-ui.svg?style=flat-square)](https://github.com/cat-land/miau-ui/issues)
[![GitHub forks](https://img.shields.io/github/forks/cat-land/miau-ui.svg?style=flat-square)](https://github.com/cat-land/miau-ui/network)
[![GitHub last commit](https://img.shields.io/github/last-commit/cat-land/miau-ui.svg?style=flat-square)](https://github.com/cat-land/miau-ui)
[![license](https://img.shields.io/github/license/mashape/apistatus.svg?style=flat-square)](https://github.com/cat-land/miau-ui)

# miau-ui

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
